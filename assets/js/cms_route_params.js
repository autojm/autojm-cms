import $ from 'jquery';

function buildTable($el, params) {
  let defaults;
  let requirements;

  if ($('.cms_route_default_input').val() === '[]' || $('.cms_route_default_input').val() === '') {
    defaults = {};
  } else {
    defaults = JSON.parse($('.cms_route_default_input').val());
  }

  if ($('.cms_route_requirements_input').val() === '[]' || $('.cms_route_requirements_input').val() === '') {
    requirements = {};
  } else {
    requirements = JSON.parse($('.cms_route_requirements_input').val());
  }

  const $entitiesInput = $('.cms_route_entities_input');
  const entities = $entitiesInput && $entitiesInput.val() && $entitiesInput.val() !== '[]' ? JSON.parse($entitiesInput.val()) : {};

  $.each(defaults, (param) => {
    if (!params.includes(param)) {
      delete defaults[param];
    }
  });
  $('.cms_route_default_input').val(JSON.stringify(defaults));
  $.each(requirements, (param) => {
    if (!params.includes(param)) {
      delete requirements[param];
    }
  });
  $('.cms_route_requirements_input').val(JSON.stringify(requirements));

  $.each(entities, (param) => {
    if (!params.includes(param)) {
      delete entities[param];
    }
  });
  $entitiesInput.val(JSON.stringify(entities));

  const div = $('<div>', {
    class: 'form-group path_params_container',
  });
  const table = $('<table>', { class: 'table table-bordered' });
  const thead = $('<thead></thead>');
  const thead_tr = $('<tr></tr>');
  thead.append(thead_tr);
  thead_tr.append('<th>Paramètre</th><th>Entité</th><th>Critère</th><th>Défaut</th>');
  const tbody = $('<tbody>');

  params.forEach((params) => {
    const tr = $('<tr>');
    tr.append($(`<td>${params}</td>`));
    const inputRequirement = $('<input>', {
      type: 'text',
      class: 'form-control',
    });
    const inputDefault = $('<input>', {
      type: 'text',
      class: 'form-control',
    });
    const inputEntity = $('<input>', {
      type: 'text',
      class: 'form-control',
    });

    inputDefault.on('change', function () {
      defaults[params] = $(this).val() === '' ? null : $(this).val();
      $('.cms_route_default_input').val(JSON.stringify(defaults));
    });

    inputRequirement.on('change', function () {
      requirements[params] = $(this).val();
      $('.cms_route_requirements_input').val(JSON.stringify(requirements));
    });

    inputEntity.on('change', function () {
      entities[params] = $(this).val();
      $('.cms_route_entities_input').val(JSON.stringify(entities));
    });

    if (defaults.hasOwnProperty(params)) {
      inputDefault.val(defaults[params]);
    }
    if (requirements.hasOwnProperty(params)) {
      inputRequirement.val(requirements[params]);
    }
    if (entities.hasOwnProperty(params)) {
      inputEntity.val(entities[params]);
    }

    tr.append($('<td></td>').append(inputEntity));
    tr.append($('<td></td>').append(inputRequirement));
    tr.append($('<td></td>').append(inputDefault));
    tbody.append(tr);
  });
  table.append(thead, tbody);
  $('.path_params_container').remove();
  div.append(table);
  $el.parent().parent().after(div);
}

// parse le champ pour trouver tous les params '{params}'
function parsePath($el) {
  const val = $el.val();

  const regex = /\{\w+\}/gm;
  let m;
  const vars = [];
  while ((m = regex.exec(val)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    // The result can be accessed through the `m`-variable.
    m.forEach((match, groupIndex) => {
      vars.push(match.replace(/[\{\}]/g, ''));
    });
  }

  buildTable($el, vars);
}

$(document).ready(() => {
  $.each($('.cms_route_path_input'), function () {
    $(this).on('change blur', function () {
      parsePath($(this));
    });
    parsePath($(this));
  });
});
