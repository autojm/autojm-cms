require('../sass/content_collapse.scss');

document.addEventListener('DOMContentLoaded', () => {
  const collapseGlobal = document.querySelector('#contents_collapse');
  if (collapseGlobal != null && collapseGlobal !== undefined) {
    const collapses = collapseGlobal.querySelectorAll('.collapse');

    collapses.forEach((collapse) => {
      $(collapse).on('shown.bs.collapse', () => {
        Admin.setup_sticky_elements(document);
      });
    });
  }

  const vars = document.querySelectorAll('.content-var');
  vars.forEach((v) => {
    v.addEventListener('click', (e) => {
      navigator.clipboard.writeText(v.innerHTML);

      const msg = document.createElement('div');
      msg.classList.add('msg-dialog');
      msg.style.cssText = `left: ${e.clientX}px; top: ${e.clientY - 25}px`;
      msg.innerHTML = 'Copié';

      document.body.appendChild(msg);
      setTimeout(function () {
        msg.remove();
      }, 1500);
    });
  });
});
