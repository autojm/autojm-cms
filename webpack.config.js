require('dotenv').config();

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

const ENV = process.env.APP_ENV;
const isTest = ENV === 'test';
const isProd = ENV === 'prod';

function setDevTool() {
    if (isTest) {
        return 'inline-source-map';
    } else if (isProd) {
        return 'source-map';
    } else {
        return 'eval-source-map';
    }
}

const config = {
    entry: path.resolve('./assets/js/app.js'),
    mode: isProd ? "production" : 'development',
    output: {
        path: path.resolve('./src/Resources/public'),
        filename: 'cms_admin.js',
    },
    devtool: setDevTool(),
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: [
                    /node_modules/,
                ],
            },
            {
                test: /\.(sass|scss)$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin(),
        new webpack.IgnorePlugin({resourceRegExp: /^\.\/locale$/, contextRegExp: /moment$/}),
        new CopyWebpackPlugin({
            patterns: [
                {from: path.resolve('./assets/img'), to: path.resolve('./src/Resources/public/img')}
            ]
        }),
    ],
};

module.exports = config;
