<?php


namespace AutoJM\CmsBundle\Services;


use Knp\Menu\ItemInterface;

interface CmsMenuBuilderInterface
{

    public function build(ItemInterface $node, $locale);

}
