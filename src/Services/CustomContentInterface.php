<?php

namespace AutoJM\CmsBundle\Services;

use Symfony\Component\Form\DataTransformerInterface;
use AutoJM\CmsBundle\Entity\CmsContent;

interface CustomContentInterface
{
    public function getFormOptions(): array;

    public function getFormType(): string;

    public function getCallbackTransformer(): DataTransformerInterface;

    public function render(CmsContent $content);

    public function isSet(CmsContent $content);
}
