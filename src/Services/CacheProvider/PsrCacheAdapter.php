<?php

namespace AutoJM\CmsBundle\Services\CacheProvider;

use Psr\Cache\CacheItemPoolInterface;

class PsrCacheAdapter implements CacheProviderInterface
{
    private CacheItemPoolInterface $cache;

    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $key
     * @return mixed|false
     */
    public function get($key)
    {
        // PSR-6 implementation returns null, CacheProviderInterface expects false
        $item = $this->cache->getItem($key);
        if ($item->isHit()) {
            return $item->get();
        }
        return false;
    }

    /**
     * @param string $key
     * @param string $value
     * @param int|\DateInterval $lifetime
     * @return bool
     */
    public function set($key, $value, $lifetime = 1800)
    {
        $item = $this->cache->getItem($key);
        $item->set($value);
        $item->expiresAfter($lifetime);

        return $this->cache->save($item);
    }

    public function remove($key)
    {
        return $this->cache->deleteItem($key);
    }

    public function clear()
    {
        return $this->cache->clear();
    }
}
