<?php

namespace AutoJM\CmsBundle\Services\CacheProvider;

interface CacheProviderInterface
{
    public function set($code, $content);

    public function get($code);

    public function remove($code);

    public function clear();
}
