<?php

namespace AutoJM\CmsBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use AutoJM\CmsBundle\Entity\CmsPage;
use AutoJM\CmsBundle\Entity\CmsPageDeclination;
use AutoJM\CmsBundle\Repository\CmsPageRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CmsHelper
{
    private $em;

    private $provider;

    private $twig;

    private AuthorizationCheckerInterface $authorizationChecker;

    private $page;

    private $roleHierarchy;

    private $tokenStorage;

    /**
     * @param EntityManagerInterface $em
     * @param TemplateProvider $provider
     * @param Environment $twig
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RoleHierarchyInterface $roleHierarchy
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        EntityManagerInterface        $em,
        TemplateProvider              $provider,
        Environment                   $twig,
        AuthorizationCheckerInterface $authorizationChecker,
        RoleHierarchyInterface        $roleHierarchy,
        TokenStorageInterface         $tokenStorage
    ) {
        $this->em                   = $em;
        $this->provider             = $provider;
        $this->twig                 = $twig;
        $this->authorizationChecker = $authorizationChecker;
        $this->roleHierarchy        = $roleHierarchy;
        $this->tokenStorage         = $tokenStorage;
    }

    /**
     * @return CmsPage|null
     */
    public function getPage(Request $request)
    {
        if ($this->page === null) {
            /** @var CmsPageRepository $repository */
            $repository = $this->em->getRepository(CmsPage::class);
            $this->page = $repository->findByRouteName($request->attributes->get('_route'));
        }

        return $this->page;
    }

    public function getDeclinaison(Request $request)
    {
        $path = $request->getRequestUri();

        $path = preg_replace('(\?.*)', '', $path);
        $path = preg_replace('/\.([a-z]+)$/', '', $path);

        $page = $this->getPage($request);
        if (!$page) {
            return null;
        }

        $declinations = $page->getDeclinations()->toArray();

        uasort($declinations, function (CmsPageDeclination $a, CmsPageDeclination $b) {
            return strlen($a->getPath()) < strlen($b->getPath());
        });

        /** @var CmsPageDeclination $declination */
        foreach ($declinations as $declination) {
            $dPath = $declination->getPath();

            if (!empty($page->getSite()->getLocale())) {
                $dPath = '/' . $page->getSite()->getLocale() . $dPath;
            }

            if (preg_match('/^' . preg_quote($dPath, '/') . '/', $path)) {
                return $declination;
            }
        }

        return null;
    }


    /**
     * @param Request $request
     * @param array $params
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @deprecated since 1.2.0, use CmsController::defaultRender instead
     */
    public function getDefaultRender(Request $request, array $params): Response
    {
        /** @var CmsPage $page */
        $page = $this->getPage($request);

        $request->setLocale($this->getLocale($request));

        return new Response(
            $this->twig->render(
                $this->provider->getTemplate($page->getTemplate()),
                array_merge(
                    $params,
                    [
                        'page' => $page,
                    ]
                )
            )
        );
    }

    public function getLocale(Request $request)
    {
        /** @var CmsPage $page */
        $page = $this->getPage($request);

        if (!$page) {
            return null;
        }

        return $page->getSite() ? $page->getSite()->getLocale() : null;
    }

    public function isGranted(Request $request): bool
    {
        /** @var CmsPage $page */
        $page = $this->getPage($request);

        if (!$page) {
            return true;
        }

        if (count($page->getRoles()) < 1) {
            return true;
        }

        $roles = $this->roleHierarchy->getReachableRoleNames($page->getRoles());

        foreach ($roles as $role) {
            if ($this->authorizationChecker->isGranted($role)) {
                return true;
            }
        }

        /**
         * Permet de mettre un role anonyme valable que pour les users non LOG. Ne marche pas avec IS_AUTHENTICATED_ANONYMOUSLY
         * @TODO : remove en SF5
         **/
        if ($this->tokenStorage->getToken() !== null) {
            if ((in_array('IS_ANONYMOUS', $roles) && $this->tokenStorage->getToken()->getUser() === 'anon.')) {
                return true;
            }
        }

        return false;
    }
}
