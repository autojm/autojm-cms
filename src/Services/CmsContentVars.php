<?php

namespace AutoJM\CmsBundle\Services;

use AutoJM\CmsBundle\Entity\CmsGlobalVarsDelimiterEnum;
use AutoJM\CmsBundle\Entity\CmsRoute;
use AutoJM\CmsBundle\Entity\GlobalVarsInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class CmsContentVars
{
    /** @var GlobalVarsInterface[] */
    protected array $objects = [];

    protected array $entitiesVars = [];

    protected array $values = [];

    protected array $requestValues = [];

    protected static $delimiter = CmsGlobalVarsDelimiterEnum::DOUBLE_UNDERSCORE;

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function inject(CmsRoute $route, $request)
    {
        $attributes = $route->getAttributes();
        $entites = $route->normalizeEntities();

        foreach ($attributes as $attribute) {
            $this->requestValues[$attribute] = $request[$attribute] ?? null;
        }

        $objects = $this->convert($entites, $request);
        $this->addObjectsVars($objects);
    }

    private function convert($entities, $request)
    {
        if (!$entities) {
            return [];
        }

        $objects = [];
        foreach ($entities as $key => $data) {
            $value = $request[$key] ?? null;

            $property = $data['property'] ?? 'id';

            $repo = $this->em->getRepository($data['entity']);
            $object = $repo->findOneBy([
                $property => $value
            ]);

            if (!$object) {
                throw EntityNotFoundException::fromClassNameAndIdentifier($data['entity'], [
                    $property => $value
                ]);
            }

            $objects[] = $object;
        }

        return $objects;
    }

    public function computeValues()
    {
        $values = [];
        foreach ($this->objects as $object) {
            $objectVars = $object::getAvailableVars();
            foreach ($objectVars as $var) {
                $values[$var] = $object->$var();
            }
        }

        foreach ($this->requestValues as $k => $v) {
            $values[$k] = $v;
        }

        $this->values = $values;
    }

    public function replaceVars($str)
    {
        if (empty($this->values)) {
            $this->computeValues();
        }

        $d = self::getDelimiters();
        foreach ($this->values as $name => $value) {
            $str = str_replace($d['s'] . $name . $d['e'], $value, $str);
        }

        return $str;
    }

    public function getRouteParams($routeParams)
    {
        $params = [];
        foreach ($this->objects as $object) {
            if (method_exists($object, 'getRouteParam')) {
                /** @var CmsRoute $route */
                foreach ($routeParams as $param) {
                    $value = $object->getRouteParam($param);
                    if ($value) {
                        $params[$param] = $value;
                    }
                }
            }
        }

        return $params;
    }

    public function addGlobalVars(GlobalVarsInterface $vars)
    {
        $this->objects[] = $vars;
    }

    public function addObjectVars($object)
    {
        foreach ($this->entitiesVars as $c => $entitiesVar) {
            if ($object instanceof $c) {
                $this->objects[] = new $entitiesVar($object);
            }
        }
    }

    public function addObjectsVars($objects)
    {
        if (is_array($objects)) {
            foreach ($objects as $object) {
                $this->addObjectVars($object);
            }
        }
    }

    public function setEntitiesVars($entitiesVars)
    {
        $this->entitiesVars = $entitiesVars;
    }

    public static function getDelimiters()
    {
        switch (self::getDelimiter()) {
            case CmsGlobalVarsDelimiterEnum::DOUBLE_UNDERSCORE:
                $de = $ds = '__';
                break;
            case CmsGlobalVarsDelimiterEnum::DOUBLE_SQUARE_BRACKETS:
                $ds = '[[';
                $de = ']]';
                break;
            case CmsGlobalVarsDelimiterEnum::SQUARE_BRACKETS:
                $ds = '[';
                $de = ']';
                break;
        }

        return ['s' => $ds, 'e' => $de];
    }

    /**
     * @return mixed
     */
    public static function getDelimiter()
    {
        return self::$delimiter;
    }


    /**
     * @param mixed $delimiter
     */
    public static function setDelimiter($delimiter): void
    {
        self::$delimiter = $delimiter;
    }
}
