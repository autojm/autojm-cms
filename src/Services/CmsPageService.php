<?php

namespace AutoJM\CmsBundle\Services;

use AutoJM\CmsBundle\Entity\CmsPage;
use AutoJM\CmsBundle\Entity\CmsPageDeclination;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CmsPageService
{
    private ?CmsPage $page = null;
    private ?CmsPageDeclination $declinaison = null;
    private $configCms;
    private CmsContentService $ccs;

    public function __construct(CmsContentService $ccs, ParameterBagInterface  $parameterBag)
    {
        $this->ccs = $ccs;
        $this->configCms = $parameterBag->get('ajm_cms.cms');
    }

    public function setCurrentPage(?CmsPage $page = null)
    {
        $this->page = $page;
    }

    public function setCurrentDeclinaison(CmsPageDeclination $declinaison = null)
    {
        $this->declinaison = $declinaison;
    }

    public function getObject($object = null)
    {
        if (!$object) {
            $object = $this->page;
        }

        if ($object instanceof CmsPage && $this->page === $object) {
            $object = $this->declinaison !== null ? $this->declinaison : $object;
        }

        return $object;
    }
    public function getContent($code, $object = null)
    {
        $content = $this->ccs->renderContent($this->getObject($object), $code);

        return $content;
    }

    public function getSharedBlock($code)
    {
        $siteId = $this->configCms['multilingual'] ? $this->getSiteId() : null;
        $content = $this->ccs->renderSharedBlock($code, $siteId);

        return $content;
    }
    public function getSiteId($page = null)
    {
        $object = $page ? $page : $this->page;

        return $object ? $object->getSite()->getId() : null;
    }
    public function getPathInfo($page = null)
    {
        $object = $page ? $page : $this->getObject();
        $path = '';
        if ($object instanceof CmsPageDeclination) {
            $path .= 'd';
        }
        $path .= $object->getId();

        return $path;
    }

    public function getSeoSmoValue($value)
    {
        return $this->ccs->replaceVarContent($value);
    }
}
