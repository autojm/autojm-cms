<?php

namespace AutoJM\CmsBundle\Services;

use Symfony\Component\Form\DataTransformerInterface;
use AutoJM\CmsBundle\Entity\CmsContent;

abstract class AbstractCustomContent implements CustomContentInterface
{
    abstract function getFormOptions(): array;

    abstract function getFormType(): string;

    abstract function getCallbackTransformer(): DataTransformerInterface;

    abstract function render(CmsContent $content);

    public function isSet(CmsContent $content)
    {
        return true;
    }
}
