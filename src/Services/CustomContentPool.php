<?php

namespace AutoJM\CmsBundle\Services;

use AutoJM\CmsBundle\Entity\CmsContentTypeEnum;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class CustomContentPool
{
    /**
     * @var array
     */
    private $customContents = [];

    private ContainerInterface $container;

    public function __construct($customContentConfig, ContainerInterface $container)
    {
        foreach ($customContentConfig as $type => $value) {
            $this->addCustomContent($type, $value['service']);
        }
        $this->container = $container;
    }

    public function get($type): ?CustomContentInterface
    {
        $serviceName = $this->customContents[$type] ?? null;
        if (!$serviceName) {
            return null;
        }

        return $this->container->get($serviceName);
    }

    public function getTypes()
    {
        return array_merge([
            CmsContentTypeEnum::TEXT,
            CmsContentTypeEnum::TEXTAREA,
            CmsContentTypeEnum::WYSYWYG,
            CmsContentTypeEnum::SHARED_BLOCK,
            CmsContentTypeEnum::SHARED_BLOCK_COLLECTION,
            CmsContentTypeEnum::MEDIA,
            CmsContentTypeEnum::IMAGE,
            CmsContentTypeEnum::CHECKBOX,
            CmsContentTypeEnum::COLOR,
        ], array_keys($this->customContents));
    }

    public function addCustomContent($type, $serviceName)
    {
        $this->customContents[$type] = $serviceName;
    }
}
