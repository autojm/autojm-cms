<?php

namespace AutoJM\CmsBundle\Services;

use AutoJM\CmsBundle\Entity\CmsContent;
use Twig\Environment;
use AutoJM\CmsBundle\Entity\CmsPageDeclination;
use AutoJM\CmsBundle\Repository\CmsContentRepository;
use AutoJM\CmsBundle\Repository\CmsSharedBlockRepository;

class CmsContentService
{
    private Environment $twig;
    private TemplateProvider $templateProvider;
    private CmsSharedBlockRepository $cmsSharedBlockRepository;
    private CustomContentPool $contentPool;
    private CmsContentRepository $cmsContentRepository;
    private CmsContentVars $contentVars;

    public function __construct(
        Environment $twig,
        CmsSharedBlockRepository $cmsSharedBlockRepository,
        TemplateProvider $templateProvider,
        CustomContentPool $contentPool,
        CmsContentRepository $cmsContentRepository,
        CmsContentVars $contentVars
    ) {
        $this->twig = $twig;
        $this->templateProvider = $templateProvider;
        $this->cmsSharedBlockRepository = $cmsSharedBlockRepository;
        $this->contentPool = $contentPool;
        $this->cmsContentRepository = $cmsContentRepository;
        $this->contentVars = $contentVars;
    }

    public function renderContent($object, $code)
    {
        $content = $this->getContent($object, $code);
        if ($content === null) {
            return null;
        }

        $contentService = $this->contentPool->get($content->getType());
        $content->setValue($this->replaceVarContent($content->getValue()));

        return $contentService !== null ? $contentService->render($content) : $content->getValue();
    }

    public function contentIsSet($content)
    {
        $contentService = $this->contentPool->get($content->getType());
        if ($contentService) {
            return $contentService->isSet($content);
        }

        return true;
    }

    public function getContent($object, $code): ?CmsContent
    {
        $content = $this->retrieveContent($object, $code);
        if (!$content || !$content->isSet() || !$this->contentIsSet($content)) {
            if ($object instanceof CmsPageDeclination) {
                $page = $object->getPage();
                $content = $this->retrieveContent($page, $code);
            }
        }

        return $content;
    }

    private function retrieveContent($object, $code)
    {
        $content = $this->cmsContentRepository
            ->findOneByObjectAndContentCodeAndType(
                $object,
                $code,
                $this->contentPool->getTypes()
            );

        return $content;
    }

    public function renderSharedBlock($code, $siteId = null)
    {
        $block = $this->getSharedBlock($code, $siteId);
        if (!$block) {
            return null;
        }

        return $this->twig->render(
            $this->templateProvider->getConfigurationFor($block->getTemplate())['template'],
            [
                'block' => $block,
            ]
        );
    }

    public function getSharedBlock($code, $siteId = null)
    {
        if ($siteId) {
            $args = [
                'code' => $code,
                'site' => $siteId,
            ];
        } else {
            $args = ['code' => $code];
        }

        return $this->cmsSharedBlockRepository->findOneBy($args);
    }

    public function replaceVarContent($value)
    {
        return $this->contentVars->replaceVars($value);
    }
}
