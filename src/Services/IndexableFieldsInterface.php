<?php


namespace AutoJM\CmsBundle\Services;


use AutoJM\CmsBundle\Entity\CmsContent;

interface IndexableFieldsInterface
{
    public function getIndexableFieldData(): string;
}
