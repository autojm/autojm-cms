<?php

namespace AutoJM\CmsBundle\Services;

use AutoJM\CmsBundle\Entity\CmsPage;
use AutoJM\CmsBundle\Entity\CmsPageDeclination;
use AutoJM\CmsBundle\Entity\CmsRoute;
use Exception;
use Symfony\Component\Routing\RouterInterface;

class BreadcrumbService
{
    private CmsContentVars $globalVars;
    protected RouterInterface $router;

    public function __construct(CmsContentVars $globalVars, RouterInterface $router)
    {
        $this->globalVars = $globalVars;
        $this->router = $router;
    }

    /**
     * @param CmsPage|CmsPageDeclination $page
     * @return array
     */
    public function generateBreadcrumb($page): array
    {
        return $this->getBreadcrumbPage($page);
    }
    /**
     * @param CmsPage|CmsPageDeclination $page
     * @return array
     */
    private function getBreadcrumbPage($page, &$breadcrumb = [])
    {
        if ($page->getParent()) {
            $this->getBreadcrumbPage($page->getParent(), $breadcrumb);
        }

        $breadcrumb[] = [
            'label' => $this->globalVars->replaceVars($page->getSeoBreadcrumb() ?: $page->getSeoTitle()),
            'link'  => $this->generateRoute($page->getRoute()),
        ];

        return $breadcrumb;
    }

    public function generateRoute(CmsRoute $route)
    {
        $params = $this->globalVars->getRouteParams($route->getParams());

        try {
            return $this->router->generate($route->getName(), $params);
        } catch (Exception $e) {
            return '';
        }
    }
}
