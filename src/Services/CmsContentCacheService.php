<?php

namespace AutoJM\CmsBundle\Services;

use AutoJM\CmsBundle\Entity\CmsContent;
use Symfony\Component\HttpFoundation\RequestStack;
use AutoJM\CmsBundle\Services\CacheProvider\CacheProviderInterface;

class CmsContentCacheService
{
    private CacheProviderInterface $cacheProvider;

    private CmsPageService $cmsPageService;

    private RequestStack $rs;

    public function __construct(CacheProviderInterface $cacheProvider, CmsPageService $cmsPageService, RequestStack $rs)
    {
        $this->cacheProvider = $cacheProvider;
        $this->cmsPageService = $cmsPageService;
        $this->rs = $rs;
    }

    public function getCmsContentCache($c, $sharedBlock = false)
    {
        $code = $this->generateCode([
            'code' => $c,
            'sharedBlock' => $sharedBlock
        ]);

        return $this->cacheProvider->get($code);
    }

    public function createCmsContentCache($c, $content, $sharedBlock = false)
    {
        $code = $this->generateCode([
            'code' => $c,
            'sharedBlock' => $sharedBlock
        ]);

        $this->cacheProvider->set($code, $content);
    }

    public function removeCmsContentCache($object)
    {
        $code = $this->generateCode($object, false);

        return $this->cacheProvider->remove($code);
    }

    public function removeAll()
    {
        return $this->cacheProvider->clear();
    }

    /**
     * @param CmsContent|array
     */
    public function generateCode($object, $withPath = true)
    {
        return $object instanceof CmsContent ?
            $this->generateByObject($object, $withPath) :
            $this->generateByArray($object, $withPath);
    }

    public function generateByObject($object, $withPath)
    {
        $sharedBlock = $object->getSharedBlockParent();
        if ($sharedBlock !== null) {
            $siteId = $object->getSiteId();
            $code =  $siteId . '_' . $sharedBlock->getCode();
        } else {
            $siteId = $object->getPage()->getSite()->getId();
            $path = $withPath ? $this->getPathInfo() : '';
            $code = $siteId . '_' . $object->getCode() . '_' . $path;
        }

        return $code;
    }

    public function generateByArray($arr, $withPath)
    {
        $siteId = $this->cmsPageService->getSiteId();
        if ($arr['sharedBlock']) {
            $code = $siteId . '_' . $arr['code'];
        } else {
            $path = $withPath ? $this->getPathInfo() : '';
            $code = $siteId . '_' . $arr['code'] . '_' . $path;
        }

        return $code;
    }

    private function getPathInfo()
    {
        if (!$this->rs->getCurrentRequest()) {
            return '';
        }

        return md5(substr($this->rs->getCurrentRequest()->getPathInfo(), 1));
    }
}
