<?php

namespace AutoJM\CmsBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CmsContentVoter extends Voter
{
    public const CAN_MANAGE_CONTENT = 'CAN_MANAGE_CONTENT';

    protected function supports($attribute, $subject): bool
    {
        return $attribute === self::CAN_MANAGE_CONTENT;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            // the user must be logged in; if not, deny access
            return false;
        }

        return $user->hasRole('ROLE_ADMIN_CMS');
    }

}
