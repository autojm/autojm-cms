<?php

namespace AutoJM\CmsBundle\Twig\CustomContent;

use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

class CustomContentTokenParser extends AbstractTokenParser
{
    public function parse(Token $token)
    {
        list($contentCode, $template) = $this->parseArguments();

        return new CustomContentNode($contentCode, $template, $token->getLine(), $this->getTag());
    }

    protected function parseArguments()
    {
        $stream = $this->parser->getStream();

        $contentCode = $this->parser->getExpressionParser()->parseExpression();
        $template = $this->parser->getExpressionParser()->parseExpression();

        $stream->expect(/* Token::BLOCK_END_TYPE */3);

        return [$contentCode, $template];
    }

    public function getTag()
    {
        return 'cms_content';
    }
}
