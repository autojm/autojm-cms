<?php

namespace AutoJM\CmsBundle\Twig\CustomContent;

use Twig\Compiler;
use Twig\Node\Node;
use Twig\Node\NodeOutputInterface;

class CustomContentNode extends Node implements NodeOutputInterface
{

    protected static $cmsCacheExtension = 'AutoJM\CmsBundle\Twig\CmsCacheExtension';
    protected static $cmsTwigExtension = 'AutoJM\CmsBundle\Twig\CmsTwigExtension';

    /**
     * @param string  $contentCode      An array of named nodes
     * @param string  $template      An array of named nodes
     * @param int    $line      The line number
     * @param string $tag        The tag name associated with the Node
     */
    public function __construct($contentCode, $template, $line, $tag = null)
    {
        parent::__construct(['contentCode' => $contentCode, 'template' => $template], [], $line, $tag);
    }

    public function compile(Compiler $compiler)
    {

        $compiler->addDebugInfo($this);

        $compiler
            ->write('$templateName =')
            ->subcompile($this->getNode('template'))
            ->raw(';')
            ->write('$code =')->write('$code =')
            ->subcompile($this->getNode('contentCode'))
            ->raw(';')
            ->write('$cccs = $this->env->getExtension("' . self::$cmsCacheExtension . '")->getCacheService();')
            ->write('$cache = $cccs->getCmsContentCache($code);')
            ->write('if(!$cache) {')
            ->write('$content = $this->env->getExtension("' . self::$cmsTwigExtension . '")->cmsContent($code);')
            ->write('if($templateName) {')
            ->write('$template = $this->loadTemplate($templateName);')
            ->write('$cache = $template->render(["content" => $content]);')
            ->write('} else { $cache = $content;}')
            ->write('if($cache) {')
            ->write('$cccs->createCmsContentCache($code, $cache);')
            ->write('}')
            ->write('}')
            ->write('echo $cache;');
    }
}
