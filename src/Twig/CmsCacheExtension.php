<?php

namespace AutoJM\CmsBundle\Twig;

use Twig\Extension\AbstractExtension;
use AutoJM\CmsBundle\Services\CmsContentCacheService;

class CmsCacheExtension extends AbstractExtension
{
    private CmsContentCacheService $cccs;

    public function __construct(CmsContentCacheService $cccs)
    {
        $this->cccs = $cccs;
    }

    public function getCacheService()
    {
        return $this->cccs;
    }
}
