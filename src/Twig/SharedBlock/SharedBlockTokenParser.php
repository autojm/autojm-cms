<?php

namespace AutoJM\CmsBundle\Twig\SharedBlock;

use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

class SharedBlockTokenParser extends AbstractTokenParser
{
    public function parse(Token $token)
    {

        list($contentCode) = $this->parseArguments();

        return new SharedBlockNode($contentCode, $token->getLine(), $this->getTag());
    }

    protected function parseArguments()
    {
        $stream = $this->parser->getStream();

        
        $contentCode = $this->parser->getExpressionParser()->parseExpression();

        $stream->expect(/* Token::BLOCK_END_TYPE */3);

        return [$contentCode];
    }

    public function getTag()
    {
        return 'cms_shared_block';
    }
}
