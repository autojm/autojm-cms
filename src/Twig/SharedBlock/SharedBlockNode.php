<?php

namespace AutoJM\CmsBundle\Twig\SharedBlock;

use Twig\Compiler;
use Twig\Node\Node;
use Twig\Node\NodeOutputInterface;

class SharedBlockNode extends Node implements NodeOutputInterface
{

    protected static $cmsCacheExtension = 'AutoJM\CmsBundle\Twig\CmsCacheExtension';
    protected static $cmsTwigExtension = 'AutoJM\CmsBundle\Twig\CmsTwigExtension';

    /**
     * @param string  $contentCode      An array of named nodes
     * @param int    $line      The line number
     * @param string $tag        The tag name associated with the Node
     */
    public function __construct($contentCode, $line, $tag = null)
    {
        parent::__construct(['contentCode' => $contentCode], [], $line, $tag);
    }

    public function compile(Compiler $compiler)
    {

        $compiler->addDebugInfo($this);

        $compiler
            ->write('$cccs = $this->env->getExtension("' . self::$cmsCacheExtension . '")->getCacheService();')
            ->write('$cache = $cccs->getCmsContentCache(')
            ->subcompile($this->getNode('contentCode'))
            ->raw(', ')
            ->write('true')
            ->raw(');')
            ->write('if(!$cache) {')
            ->write('$cache = $this->env->getExtension("' . self::$cmsTwigExtension . '")->cmsRenderSharedBlock(')
            ->subcompile($this->getNode('contentCode'))
            ->raw(');')
            ->write('$cccs->createCmsContentCache(')
            ->subcompile($this->getNode('contentCode'))
            ->raw(', ')
            ->write('$cache')
            ->raw(', ')
            ->write('true')
            ->raw(');')
            ->write('}')
            ->write('echo $cache;');
    }
}
