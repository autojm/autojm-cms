<?php

namespace AutoJM\CmsBundle\Twig;

use Exception;
use Twig\TwigTest;
use Twig\Environment;
use Twig\TwigFunction;
use AutoJM\CmsBundle\Entity\CmsPage;
use Twig\Extension\AbstractExtension;
use Doctrine\ORM\EntityManagerInterface;
use AutoJM\CmsBundle\Entity\CmsSharedBlock;
use AutoJM\CmsBundle\Services\BreadcrumbService;
use Symfony\Component\Routing\RouterInterface;
use AutoJM\CmsBundle\Services\TemplateProvider;
use Symfony\Component\HttpFoundation\RequestStack;
use AutoJM\CmsBundle\Services\CmsPageService;
use AutoJM\CmsBundle\Twig\CustomContent\CustomContentTokenParser;
use AutoJM\CmsBundle\Twig\SharedBlock\SharedBlockTokenParser;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Contracts\Translation\TranslatableInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CmsTwigExtension extends AbstractExtension
{
    private $requestStack;
    private $pageProvider;
    private $twig;
    private $em;
    private $router;
    private $configCms;
    private CmsPageService $cps;
    private BreadcrumbService $breadcrumbService;

    /**
     * @inheritDoc
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface        $router,
        Environment            $twig,
        TemplateProvider       $pageProvider,
        RequestStack           $requestStack,
        ParameterBagInterface  $parameterBag,
        CmsPageService         $cps,
        BreadcrumbService      $breadcrumbService
    ) {
        $this->em            = $entityManager;
        $this->router        = $router;
        $this->twig          = $twig;
        $this->pageProvider  = $pageProvider;
        $this->requestStack  = $requestStack;
        $this->configCms     = $parameterBag->get('ajm_cms.cms');
        $this->cps           = $cps;
        $this->breadcrumbService = $breadcrumbService;
    }

    public function getTokenParsers()
    {
        return [
            new CustomContentTokenParser(),
            new SharedBlockTokenParser()
        ];
    }

    public function getTests()
    {
        return [
            new TwigTest('instanceOf', [$this, 'isInstanceOf']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'cms_render_content',
                [$this, 'cmsRenderContent'],
                ['is_safe' => ['html']]
            ),
            new TwigFunction(
                'cms_render_shared_block',
                [$this, 'cmsRenderSharedBlock'],
                ['is_safe' => ['html']]
            ),
            new TwigFunction("breadcrumb", [$this, "getBreadcrumb"]),
            new TwigFunction('cms_path', [$this, 'cmsPath']),
            new TwigFunction('cms_render_locale_switch', [$this, 'renderLocaleSwitch'], ['is_safe' => ['html']]),
            new TwigFunction('cms_render_meta_locale_switch', [$this, 'renderMetaLocalSwitch'], ['is_safe' => ['html']]),
            new TwigFunction('cms_render_seo_smo_value', [$this, 'renderSeoSmo']),
            new TwigFunction('route_exist', [$this, 'routeExist']),
            new TwigFunction('choice_label', [$this, 'choiceLabel']),
        ];
    }

    public function isInstanceOf($object, $class)
    {
        return $object instanceof $class;
    }

    /**
     * @param CmsPage|CmsSharedBlock $object
     * @param $content_code
     * @param $template
     * @return string|null
     * @throws Exception
     */
    public function cmsRenderContent($object, $content_code = null)
    {
        if (is_string($object)) {
            $content_code = $object;
            $object = null;
        }

        return $this->cps->getContent($content_code, $object);
    }

    public function getBreadcrumb($page)
    {
        return $this->breadcrumbService->generateBreadcrumb($page);
    }

    public function cmsContent($content_code)
    {
        return $this->cps->getContent($content_code);
    }

    public function cmsRenderSharedBlock($code)
    {
        return $this->cps->getSharedBlock($code);
    }

    public function cmsPath($route, $params = [], $absoluteUrl = false, CmsPage $page = null)
    {
        if ($this->configCms['multilingual'] && $page !== null) {
            $prefix = $page->getSite()->getLocale() . '_';
        }

        try {
            return $this->router->generate(($prefix ?? null) . $route,
                $params,
                $absoluteUrl ? UrlGeneratorInterface::ABSOLUTE_URL : UrlGeneratorInterface::ABSOLUTE_PATH
            );
        } catch (RouteNotFoundException $e) {
            return '#404(route:' . $route . ')';
        }
    }

    private function getLocalSwithPages(CmsPage $page)
    {
        $request = $this->requestStack->getCurrentRequest();

        $pages = [];

        foreach ($page->getCrossSitePages() as $p) {
            if (!$p->getSite()->isVisible() || $p->getId() === $page->getId()) {
                continue;
            }
            preg_match_all('/\{(\w+)\}/', $p->getRoute()->getPath(), $params);
            $routeParams  = [];
            $paramsConfig = $this->pageProvider->getConfigurationFor($page->getTemplate())['params'];
            foreach ($params[1] as $param) {
                if (
                    isset($paramsConfig[$param]) && isset($paramsConfig[$param]['entity']) && $paramsConfig[$param]['entity'] !== null &&
                    is_subclass_of($paramsConfig[$param]['entity'], TranslatableInterface::class)
                ) {
                    $repoMethod = 'findOneBy' . ucfirst($paramsConfig[$param]['property']);
                    $criterion  = $request->get('_route_params')[$param] ?? null;

                    $object = $this->em->getRepository($paramsConfig[$param]['entity'])
                        ->$repoMethod($criterion, $page->getSite()->getLocale());
                    if ($object) {
                        $getProperty         = 'get' . ucfirst($paramsConfig[$param]['property']);
                        $routeParams[$param] = $object->trans($p->getSite()->getLocale())->$getProperty();
                    }
                } else {
                    if (isset($paramsConfig[$param]) && isset($paramsConfig[$param]['entity']) && $paramsConfig[$param]['entity'] !== null) {
                        $getProperty         = 'get' . ucfirst($paramsConfig[$param]['property']);
                        $routeParams[$param] = $request->get($param)->$getProperty();
                    } else {
                        $routeParams[$param] = $request->get($param);
                    }
                }
            }

            try {
                $path = $this->router->generate($p->getRoute()->getName(), $routeParams);
            } catch (RouteNotFoundException $e) {
                continue;
            }

            $pages[] = [
                'path'   => $path,
                'code'   => $p->getSite()->getLocale(),
                'icon'   => $p->getSite()->getFlagIcon(),
                'locale' => $p->getSite()->getLocale(),
            ];
        }

        return $pages;
    }

    public function renderLocaleSwitch(CmsPage $page, $useless = null): ?string
    {
        $pages = $this->getLocalSwithPages($page);

        return $this->twig->render('@AjmCms/block/cms_locale_switch.html.twig', [
            'page'  => $page,
            'pages' => $pages,
        ]);
    }

    public function renderMetaLocalSwitch(CmsPage $page): ?string
    {
        $pages = $this->getLocalSwithPages($page);

        return $this->twig->render('@AjmCms/block/cms_meta_locale_switch.html.twig', [
            'page'  => $page,
            'pages' => $pages,
        ]);
    }

    public function renderSeoSmo($object, $name, $default = null)
    {
        return $this->cps->getSeoSmoValue($object->getSeoSmoValue($name, $default));
    }

    public function routeExist($path)
    {
        return (null === $this->router->getRouteCollection()->get($path)) ? false : true;
    }

    public function choiceLabel($choices, $value)
    {
        /** @var ChoiceView $choice */
        foreach ($choices as $choice) {
            if ($choice->value == $value) {
                return $choice->label;
            }
        }

        return null;
    }
}
