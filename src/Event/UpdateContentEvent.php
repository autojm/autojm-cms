<?php

namespace AutoJM\CmsBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class UpdateContentEvent extends Event
{
    private $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function getContent()
    {
        return $this->content;
    }

}
