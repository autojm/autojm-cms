<?php

namespace AutoJM\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use AutoJM\CmsBundle\Entity\CmsPage;
use AutoJM\CmsBundle\Entity\GlobalVarsInterface;
use AutoJM\CmsBundle\Services\CmsContentVars;
use AutoJM\CmsBundle\Services\CmsPageService;
use AutoJM\CmsBundle\Services\TemplateProvider;

class BaseCmsController extends AbstractController
{
    /** @var CmsPage */
    protected $page;

    /** @var CmsPageService */
    protected $cmsPage;

    /** @var string */
    protected $locale;

    /** @var boolean */
    protected $granted;

    /** @var TemplateProvider */
    protected $provider;

    /** @var CmsContentVars */
    protected $contentVars;

    private $cmsConfig;

    protected function defaultRender(array $params)
    {
        /** @var CmsPage $page */
        $page       = $this->getPage();
        $baseParams = ['page' => $page];

        $extension = $this->getExtension();
        if ($extension && !in_array($extension, ['', 'html'], true)) {
            throw $this->createNotFoundException();
        }

        if ($extension === 'html') {
            $requestStack = $this->get('request_stack');
            $request      = $requestStack->getCurrentRequest();
            $uri          = $request->getRequestUri();

            return $this->redirect(str_replace(".$extension", '', $uri), 301);
        }

        $rootDir = '/' . $page->getSite()->getTemplate() . '/';

        return $this->render($rootDir . $this->provider->getTemplate($page->getTemplate()), array_merge($params, $baseParams));
    }

    /**
     * @return string|null
     */

    private function getExtension(): ?string
    {
        /** @var RequestStack $requestStack */
        $requestStack = $this->get('request_stack');
        $request      = $requestStack->getCurrentRequest();
        $uri          = $request->getRequestUri();
        $path         = substr($uri, 0, strpos($uri, '?') ?: strlen($uri));

        preg_match('/\.([a-z]+)($|\?)/', $path, $extension);

        return $extension[1] ?? null;
    }

    /**
     * @return bool
     */
    public function isPageGranted(): bool
    {
        return $this->granted;
    }

    /**
     * @param bool $granted
     * @return CmsController
     */
    public function setGranted(bool $granted): BaseCmsController
    {
        $this->granted = $granted;

        return $this;
    }

    /**
     * @param CmsPage $page
     * @return CmsController
     */
    public function setPage(?CmsPage $page): BaseCmsController
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return CmsPage
     */
    public function getPage(): ?CmsPage
    {
        return $this->page;
    }

    /**
     * @param mixed $locale
     * @return self
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return mixed
     */
    public function getCmsConfig()
    {
        return $this->cmsConfig;
    }

    /**
     * @param mixed $cmsConfig
     * @return BaseCmsController
     */
    public function setCmsConfig($cmsConfig)
    {
        $this->cmsConfig = $cmsConfig;

        return $this;
    }

    public function addVarsObject($object)
    {
        if ($this->contentVars) {
            $this->contentVars->addObjectVars($object);
        }
    }

    public function setVarsObject(GlobalVarsInterface $object)
    {
        if ($this->contentVars) {
            $this->contentVars->addGlobalVars($object);
        }
    }

    public function setGlobalVars($contentVars): void
    {
        $this->contentVars = $contentVars;
    }

    /**
     * @return TemplateProvider
     */
    public function getProvider(): TemplateProvider
    {
        return $this->provider;
    }

    /**
     * @param TemplateProvider $provider
     */
    public function setProvider(TemplateProvider $provider): void
    {
        $this->provider = $provider;
    }

    public function setCmsPage(CmsPageService $cmsPage)
    {
        $this->cmsPage = $cmsPage;
    }
}
