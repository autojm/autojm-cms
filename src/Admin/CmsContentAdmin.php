<?php

namespace AutoJM\CmsBundle\Admin;

use Sonata\Form\Type\CollectionType;
use AutoJM\CmsBundle\Entity\CmsContent;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityManagerInterface;
use AutoJM\CmsBundle\Entity\CmsSharedBlock;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use AutoJM\CmsBundle\Entity\CmsContentTypeEnum;
use AutoJM\CmsBundle\Services\TemplateProvider;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AutoJM\CmsBundle\Security\Voter\CmsContentVoter;
use AutoJM\CmsBundle\Services\AbstractCustomContent;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class CmsContentAdmin extends AbstractAdmin
{
    protected $em;
    protected $customContents;
    protected string $media_class;
    protected ContainerInterface $container;

    private TemplateProvider $blockProvider;

    private TemplateProvider $pageProvider;

    public function __construct(
        string                 $code,
        string                 $class,
        string                 $baseControllerName,
        EntityManagerInterface $em,
        $contentTypeOption,
        string                 $media_class,
        ContainerInterface     $container,
        TemplateProvider       $blockProvider,
        TemplateProvider       $pageProvider
    ) {
        $this->em             = $em;
        $this->customContents = $contentTypeOption;
        $this->media_class    = $media_class;
        $this->container      = $container;
        $this->blockProvider  = $blockProvider;
        $this->pageProvider   = $pageProvider;

        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('active')
            ->add('code')
            ->add('label')
            ->add('type');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('id')
            ->add('active')
            ->add('code')
            ->add('label')
            ->add('type')
            ->add(
                '_action',
                null,
                [
                    'actions' => [
                        'show'   => [],
                        'edit'   => [],
                        'delete' => [],
                    ],
                ]
            );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->getFormBuilder()->setMethod('patch');

        $roleAdmin = $this->isGranted(CmsContentVoter::CAN_MANAGE_CONTENT);
        $admin     = $this;

        /** @var CmsContent $subject */
        $subject = $this->getSubject();

        $formMapper->add('active', null, [
            'label' => 'Visible',
        ]);
        $formMapper->add(
            'label',
            null,
            [
                'label' => 'Libéllé',
                'attr'  => [
                    'class' => 'admin_restriction',
                ],
            ]
        );

        if ($roleAdmin) {
            $formMapper->add('code');
            $formMapper->add(
                'type',
                ChoiceType::class,
                [
                    'choices' => $this->getContentTypeChoices(),
                ]
            );
        }

        if ($subject->getPage()) {
            $configs = $this->pageProvider->getConfigurationFor($subject->getPage()->getTemplate());
        } elseif ($subject->getDeclination() && $subject->getDeclination()->getPage()->getTemplate()) {
            $configs = $this->pageProvider->getConfigurationFor($subject->getDeclination()->getPage()->getTemplate());
        } elseif ($subject->getSharedBlockParent() && $subject->getSharedBlockParent()->getTemplate()) {
            $configs = $this->blockProvider->getConfigurationFor($subject->getSharedBlockParent()->getTemplate());
        }

        if ($subject->hasInheritContent()) {
            $formMapper->add('parent_heritance', null, [
                'label' => 'Hériter le contenu de la page parent',
                'attr'  => [
                    'class' => 'checkbox-right',
                ],
            ]);
        }

        $contents = [];
        foreach ($configs['contents'] as $content) {
            $contents[$content['code']] = $content;
        }
        $contentParams = $contents[$subject->getCode()] ?? [];

        if ($subject && $subject->getId()) {
            $subject->collapseOpen = $contentParams['open'] ?? false;

            switch ($subject->getType()) {
                case CmsContentTypeEnum::TEXT:
                    $formMapper->add('value', TextType::class, ['required' => false]);
                    $this->addHelp($formMapper, $subject, 'value');
                    break;

                case CmsContentTypeEnum::IMAGE:
                    $formMapper->add(
                        'media',
                        ModelListType::class,
                        [
                            'class'         => $this->media_class,
                            'required'      => false,
                            'model_manager' => $admin->getModelManager(),
                        ],
                        [
                            "link_parameters" => [
                                'context'  => 'cms_page',
                                'provider' => 'cms.media.provider.image',
                            ],
                        ]
                    );
                    $this->addHelp($formMapper, $subject, 'media');
                    break;

                case CmsContentTypeEnum::MEDIA:
                    $formMapper->add(
                        'media',
                        ModelListType::class,
                        [
                            'class'         => $this->media_class,
                            'required'      => false,
                            'model_manager' => $admin->getModelManager(),
                        ],
                        [
                            "link_parameters" => [
                                'context' => 'cms_page',
                            ],
                        ]
                    );
                    $this->addHelp($formMapper, $subject, 'media');
                    break;

                case CmsContentTypeEnum::WYSYWYG:
                    $options = $contentParams['options'] ?? [];

                    $formMapper->add(
                        'value',
                        SimpleFormatterType::class,
                        [
                            'format'           => 'richhtml',
                            'ckeditor_context' => $options['ckeditor_context'] ?? 'cms_page',
                            'required'         => false,
                            'auto_initialize'  => false,
                        ]
                    );
                    $this->addHelp($formMapper, $subject, 'value');
                    break;

                case CmsContentTypeEnum::TEXTAREA:
                    $formMapper->add(
                        'value',
                        TextareaType::class,
                        [
                            'required'        => false,
                            'auto_initialize' => false,
                        ]
                    );
                    $this->addHelp($formMapper, $subject, 'value');
                    break;

                case CmsContentTypeEnum::SHARED_BLOCK:
                    $formMapper->add(
                        'value',
                        EntityType::class,
                        [
                            'class'    => CmsSharedBlock::class,
                            'required' => false,
                        ]
                    );

                    $formMapper->getFormBuilder()->get('value')->addModelTransformer(new CallbackTransformer(
                        function ($value) {
                            return $this->em->getRepository(CmsSharedBlock::class)->find((int)$value);
                        },
                        function ($value) {
                            return $value !== null ? $value->getId() : null;
                        }
                    ));
                    $this->addHelp($formMapper, $subject, 'value');

                    break;
                case CmsContentTypeEnum::SHARED_BLOCK_COLLECTION:

                    $formMapper->add(
                        'sharedBlockList',
                        CollectionType::class,
                        [
                            'by_reference' => true,
                            'required'     => false,
                        ],
                        [
                            'edit'     => 'inline',
                            'inline'   => 'table',
                            'sortable' => 'position',
                        ]
                    );

                    $formMapper->add('parent_heritance', null, [
                        'label' => 'Contenu hérité',
                    ]);
                    $this->addHelp($formMapper, $subject, 'parent_heritance');
                    break;
                case CmsContentTypeEnum::CHECKBOX:
                    $formMapper->add(
                        'value',
                        CheckboxType::class,
                        ['required' => false, 'label' => false]
                    );

                    $formMapper->getFormBuilder()->get('value')->addModelTransformer(new CallbackTransformer(
                        function ($value) {
                            return filter_var($value, FILTER_VALIDATE_BOOLEAN);
                        },
                        function ($value) {
                            return $value;
                        }
                    ));

                    $this->addHelp($formMapper, $subject, 'value');
                    break;
                case CmsContentTypeEnum::COLOR:
                    $formMapper->add(
                        'value',
                        ColorType::class,
                        [
                            'required'        => false,
                            'auto_initialize' => false,
                        ]
                    );
                    $this->addHelp($formMapper, $subject, 'value');
                    break;
            }

            foreach ($this->customContents as $content => $configuration) {
                if ($subject->getType() === $content) {
                    /** @var AbstractCustomContent $contentService */
                    $contentService = $this->container->get($configuration['service']);
                    $formMapper->add(
                        'value',
                        $contentService->getFormType(),
                        $contentService->getFormOptions()
                    );

                    if (method_exists($contentService, 'getEventSubscriber')) {
                        $formMapper->getFormBuilder()->get('value')->addEventSubscriber($contentService->getEventSubscriber());
                    }

                    $formMapper->getFormBuilder()->get('value')->addModelTransformer($contentService->getCallbackTransformer());
                }
            }
        }
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('code')
            ->add('label')
            ->add('type')
            ->add('value');
    }

    public function prePersist($content)
    {
        $this->preUpdate($content);
    }

    public function preUpdate($content)
    {
    }

    protected function getContentTypeChoices()
    {
        $customs = [];
        foreach ($this->customContents as $customContent => $configuration) {
            $customs[$configuration['name']] = $customContent;
        }

        return array_merge(CmsContentTypeEnum::getChoices(), $customs);
    }

    protected function addHelp(FormMapper $formMapper, $subject, $field)
    {
        if ($subject && !empty($subject->getHelp())) {
            $formMapper->addHelp($field, $subject->getHelp());
        }
    }
}
