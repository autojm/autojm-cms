<?php

namespace AutoJM\CmsBundle\Admin;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use AutoJM\CmsBundle\Entity\CmsSite;
use AutoJM\CmsBundle\Security\Voter\CmsContentVoter;
use AutoJM\CmsBundle\Services\TemplateProvider;
use Knp\Menu\ItemInterface as MenuItemInterface;

final class CmsMenuAdmin extends AbstractAdmin
{
    protected $pageProvider;
    private $em;

    public function __construct(string $code, string $class, string $baseControllerName, EntityManager $em, TemplateProvider $pageProvider)
    {
        $this->em           = $em;
        $this->pageProvider = $pageProvider;
        parent::__construct($code, $class, $baseControllerName);
    }

    /**
     * @inheritDoc
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        if ($action === 'tree'
            && $this->getChild('cms.admin.cms_menu_item')->hasRoute('create')
        ) {
            $list['addItem'] = [
                'template' => '@AjmCms/admin/menu/actionButtons/button_create_item.html.twig',
            ];
        }

        if ($action === 'tree'
            && $this->getChild('cms.admin.cms_menu_item')->hasRoute('create')
        ) {
            $list['editMenu'] = [
                'template' => '@AjmCms/admin/menu/actionButtons/button_edit_menu.html.twig',
            ];
        }

        if ($action === 'tree'
            && $this->hasRoute('generateFromPage') && $this->isGranted(CmsContentVoter::CAN_MANAGE_CONTENT)
        ) {
            $list['generateFromPage'] = [
                'template' => "@AjmCms/admin/menu/actionButtons/button_create_form_arbo.html.twig",
            ];
        }

        return $list;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('createRootNode', 'initRoot')
            ->add('move', 'move/{id}');

        $collection->add('list', 'list/{id}', ['id' => null], ['id' => '\d*']);
        $collection->add('tree', 'tree/{id}', ['id' => null], ['id' => '\d*']);
        $collection->add('create', 'create/{id}', ['id' => null], ['id' => '\d*']);
        $collection->add('generateFromPage', 'generateFromPage/{id}', ['id' => null], ['id' => '\d*']);
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin   = $this->isChild() ? $this->getParent() : $this;
        $subject = $this->isChild() ? $this->getParent()->getSubject() : $this->getSubject();

        $id = $this->getRequest()->get('id');

        if (!$childAdmin && in_array($action, ['list', 'tree'])) {
            $sites = $this->em->getRepository(CmsSite::class)->findAll();
            if (count($sites) > 1) {
                foreach ($sites as $site) {
                    $active = $site->getId() === $this->request->attributes->get('id');
                    $menu->addChild(
                        $site->__toString(),
                        ['uri' => $admin->generateUrl('tree', ['id' => $site->getId()]), 'attributes' => ['class' => $active ? 'active' : ""]]
                    );
                }
            }
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('label')
            ->add('site');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('id')
            ->add('name')
            ->add('lft')
            ->add('lvl')
            ->add('rgt')
            ->add('_action', null, [
                'actions' => [
                    'show'   => [],
                    'edit'   => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->getFormBuilder()->setAction($this->generateUrl('create', ['id' => $this->request->attributes->get('id')]));

        $formMapper
            ->with('Configuration')
            ->add('label', null, [
                'label' => 'Nom',
            ])
            ->add('code');

        // end configuration
        $formMapper->end();
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name');
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }
}
