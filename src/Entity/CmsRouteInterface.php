<?php

/**
 * Created by PhpStorm.
 * User: Clement
 * Date: 12/02/2019
 * Time: 15:15
 */

namespace AutoJM\CmsBundle\Entity;


interface CmsRouteInterface
{
    public function getId(): ?int;

    public function setName(string $name);

    public function getName(): ?string;

    public function setMethods(array $method);

    public function getMethods(): ?array;

    public function setPath(string $path);

    public function getPath(): ?string;

    public function getAttributes(): array;

    public function getPage(): ?CmsPage;

    public function setController(?string $controller);

    public function getController(): ?string;

    public function isDynamic(): bool;

    public function setDefaults(?string $defaults);

    public function getDefaults(): ?string;

    public function setRequirements(?string $requirements);

    public function getRequirements(): ?string;

    public function setEntities(?string $entities);

    public function getEntities(): ?string;

    public function normalizeEntities(): array;

    public function getListEntities(): array;
}
