<?php

namespace AutoJM\CmsBundle\Entity;

interface GlobalVarsInterface
{
    public static function getAvailableVars(): array;

    public function getRouteParam($param);
}
