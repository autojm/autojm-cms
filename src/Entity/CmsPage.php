<?php

namespace AutoJM\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\PersistentCollection;
use AutoJM\CmsBundle\Utils\SmoOpenGraphTrait;
use AutoJM\CmsBundle\Utils\SmoTwitterTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="AutoJM\CmsBundle\Repository\CmsPageRepository")
 * @ORM\Table(name="cms__page")
 */
class CmsPage
{
    use SeoAwareTrait;
    use SmoOpenGraphTrait;
    use SmoTwitterTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     */
    private $title;

    /**
     * @var string | null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $template;

    /**
     *
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="AutoJM\CmsBundle\Entity\CmsContent", mappedBy="page", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $contents;

    /**
     * @var null | CmsRouteInterface
     *
     * @ORM\OneToOne(targetEntity="AutoJM\CmsBundle\Entity\CmsRoute", inversedBy="page", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="route_id", referencedColumnName="id", onDelete="CASCADE"))
     */
    private $route;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Gedmo\Slug(handlers={
     *      @Gedmo\SlugHandler(class="AutoJM\CmsBundle\Handler\CmsPageSlugHandler")
     * }, fields={"title"}, unique=false)
     *
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $class_association;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $query_association;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    private $association;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default" = 1})
     */
    private $indexable = true;

    /**
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $roles;

    /** @var Collection
     * @ORM\ManyToMany(targetEntity="AutoJM\CmsBundle\Entity\CmsPage")
     * @ORM\JoinTable(name="cms__page_has_page",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="associated_page_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $crossSitePages;

    private $referencePage;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AutoJM\CmsBundle\Entity\CmsPageDeclination", mappedBy="page", cascade={"persist", "remove"})
     */
    private $declinations;

    /**
     * @var Collection|null
     * @ORM\OneToMany(targetEntity="AutoJM\CmsBundle\Entity\CmsMenuItem", mappedBy="page")
     */
    private $menuItems;

    /**
     * @var CmsSite
     *
     * @ORM\ManyToOne(targetEntity="AutoJM\CmsBundle\Entity\CmsSite", inversedBy="pages")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $site;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="CmsPage")
     * @ORM\JoinColumn(name="tree_root", referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="CmsPage", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var CmsPage[]|Collection|ArrayCollection
     * @ORM\OneToMany(targetEntity="CmsPage", mappedBy="parent", cascade={"remove"})
     */
    private $children;

    private $moveMode;

    private $moveTarget;

    public $rootPage = false;

    /**
     * Set at true tu disable the creation of contents in listener, used in page import context
     * @var bool
     */
    public $dontImportContent = false;

    /**
     * Set at false tu disable the creation of route in listener
     * @var bool
     */
    public $initRoute = true;

    public $indexedContent = null;

    public function setPosition($values)
    {
        $this->setMoveMode($values['moveMode']);
        $this->setMoveTarget($values['moveTarget']);
    }

    public function getPosition()
    {
        return [
            'moveMode' => $this->getMoveMode(),
            'moveTarget' => $this->getMoveTarget()
        ];
    }

    public function getChildrenRight()
    {
        $criteria = Criteria::create()->orderBy(['rgt' => 'ASC']);

        return $this->children->matching($criteria);
    }

    public function getChildrenLeft()
    {
        $criteria = Criteria::create()->orderBy(['lft' => 'ASC']);

        return $this->children->matching($criteria);
    }

    /**
     * @return mixed
     */
    public function getClassAssociation()
    {
        return $this->class_association;
    }

    /**
     * @param mixed $class_association
     */
    public function setClassAssociation($class_association)
    {
        $this->class_association = $class_association;
    }

    /**
     * @return mixed
     */
    public function getQueryAssociation()
    {
        return $this->query_association;
    }

    /**
     * @param mixed $query_association
     */
    public function setQueryAssociation($query_association)
    {
        $this->query_association = $query_association;
    }

    /**
     * @return mixed
     */
    public function getAssociation()
    {
        return $this->association;
    }

    /**
     * @param mixed $association
     */
    public function setAssociation($association)
    {
        $this->association = $association;
    }

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        $this->contents = new ArrayCollection();
        $this->declinations = new ArrayCollection();
        $this->setActive(false);
        $this->roles          = [];
        $this->crossSitePages = new ArrayCollection();
        $this->menuItems = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return (string)$this->getTitle();
    }

    public function isRoot()
    {
        return $this->getId() == $this->getRoot()->getId();
    }

    public function isHybrid()
    {
        if ($this->getRoute()) {
            return !empty($this->getRoute()->getController());
        }

        return false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return CmsRouteInterface|null
     */
    public function getRoute(): ?CmsRouteInterface
    {
        return $this->route;
    }

    /**
     * @param null|CmsRouteInterface $route
     */
    public function setRoute(?CmsRouteInterface $route): void
    {
        $this->route = $route;
    }

    /**
     * @return ArrayCollection
     */
    public function getContents()
    {
        return $this->contents;
    }

    public function getContent($code): ?CmsContent
    {
        $criteria = new Criteria();
        $criteria
            ->where(
                Criteria::expr()->eq('code', $code)
            );

        $content = $this->contents->matching($criteria)->first() ?: null;
        if (!$content || (!$content->getValue() && $content->getParentHeritance())) {
            return $this->getParentContent($code);
        }

        return $content;
    }

    public function getParentContent($code)
    {
        return $this->getParent() ? $this->getParent()->getContent($code) : null;
    }

    /**
     * @param ArrayCollection $contents
     */
    public function setContents(ArrayCollection $contents): void
    {
        $this->contents = $contents;
    }

    public function addContent(CmsContent $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setPage($this);
        }

        return $this;
    }

    public function removeContent(CmsContent $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getPage() === $this) {
                $content->setPage(null);
            }
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getTemplate(): ?string
    {
        return $this->template;
    }

    /**
     * @param null|string $template
     */
    public function setTemplate(?string $template): void
    {
        $this->template = $template;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isIndexable(): bool
    {
        return (bool)$this->indexable;
    }

    /**
     * @param bool $indexable
     */
    public function setIndexable(bool $indexable): void
    {
        $this->indexable = $indexable;
    }

    public function getIndexable(): ?bool
    {
        return $this->indexable;
    }

    /**
     * @return array|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param array|null $roles
     * @return CmsPage
     */
    public function setRoles(?array $roles): CmsPage
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param ArrayCollection $crossSitePages
     * @return CmsPage
     */
    public function setCrossSitePages(Collection $crossSitePages): self
    {
        /** @var CmsPage $crossSitePage */
        foreach ($crossSitePages as $crossSitePage) {
            $this->addCrossSitePage($crossSitePage);
        }

        return $this;
    }

    /**
     * @return Collection|CmsPage[]
     */
    public function getCrossSitePages(): Collection
    {
        return $this->crossSitePages;
    }

    public function getCrossSitePage(CmsSite $cmsSite): ?CmsPage
    {
        foreach ($this->getCrossSitePages() as $crossSitePage) {
            if ($crossSitePage->getSite()->getId() === $cmsSite->getId()) {
                return $crossSitePage;
            }
        }
        return null;
    }

    public function addCrossSitePage(CmsPage $crossSitePage): self
    {
        if (!$this->crossSitePages->contains($crossSitePage) && $crossSitePage != $this) {
            $this->crossSitePages[] = $crossSitePage;
            $crossSitePage->addCrossSitePage($this);
            $crossSitePage->setCrossSitePages($this->crossSitePages);
        }

        return $this;
    }

    public function removeCrossSitePage(CmsPage $crossSitePage): self
    {
        if ($this->crossSitePages->contains($crossSitePage)) {
            $this->crossSitePages->removeElement($crossSitePage);
            $crossSitePage->removeCrossSitePageBis($this);
            /** @var CmsPage $v */
            foreach ($this->crossSitePages as $v) {
                //                $crossSitePage->removeCrossSitePageBis($v);
                $v->removeCrossSitePage($crossSitePage);
            }
        }

        return $this;
    }

    public function removeCrossSitePageBis(CmsPage $crossSitePage)
    {
        if ($this->crossSitePages->contains($crossSitePage)) {
            $this->crossSitePages->removeElement($crossSitePage);
        }
    }

    /**
     * @return mixed
     */
    public function getReferencePage()
    {
        return $this->referencePage;
    }

    /**
     * @param mixed $referencePage
     */
    public function setReferencePage($referencePage): void
    {
        $this->referencePage = $referencePage;
    }

    /**
     * @return ArrayCollection
     */
    public function getDeclinations()
    {
        return $this->declinations;
    }

    /**
     * @param ArrayCollection $declinations
     */
    public function setDeclinations(ArrayCollection $declinations): void
    {
        $this->declinations = $declinations;
    }

    public function addDeclination($declination): self
    {
        if (!$this->declinations->contains($declination)) {
            $this->declinations[] = $declination;
            $declination->setPage($this);
        }

        return $this;
    }

    public function removeDeclination($declination): self
    {
        if ($this->declinations->contains($declination)) {
            $this->declinations->removeElement($declination);
            // set the owning side to null (unless already changed)
            if ($declination->getPage() === $this) {
                $declination->setPage(null);
            }
        }

        return $this;
    }

    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param CmsSite $site
     */
    public function setSite($site): void
    {
        $this->site = $site;
    }

    /**
     * @return mixed
     */
    public function getMoveMode()
    {
        return $this->moveMode;
    }

    /**
     * @param mixed $moveMode
     */
    public function setMoveMode($moveMode): void
    {
        $this->moveMode = $moveMode;
    }

    /**
     * @return mixed
     */
    public function getMoveTarget()
    {
        return $this->moveTarget;
    }

    /**
     * @param mixed $moveTarget
     */
    public function setMoveTarget($moveTarget): void
    {
        $this->moveTarget = $moveTarget;
    }

    /**
     * @return mixed
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * @param mixed $lvl
     */
    public function setLvl($lvl): void
    {
        $this->lvl = $lvl;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt): void
    {
        $this->rgt = $rgt;
    }

    /**
     * @return CmsPage|null
     */
    public function getRoot(): ?CmsPage
    {
        return $this->root;
    }

    /**
     * @param CmsPage|null $root
     */
    public function setRoot(?CmsPage $root): void
    {
        $this->root = $root;
    }

    /**
     * @return CmsPage|null
     */
    public function getParent(): ?CmsPage
    {
        return $this->parent;
    }

    public function getParentAtLvl($lvl)
    {
        if ($this->getLvl() < $lvl) {
            return null;
        }

        if ($this->getLvl() === $lvl) {
            return $this;
        } else {
            return $this->getParent()->getParentAtLvl($lvl);
        }
    }

    /**
     * @param CmsPage|null $parent
     */
    public function setParent(?CmsPage $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function addChild(CmsPage $page): self
    {
        if (!$this->children->contains($page)) {
            $this->children[] = $page;
            $page->setParent($this);
        }

        return $this;
    }

    public function removeChild(CmsPage $page): self
    {
        if ($this->children->contains($page)) {
            $this->children->removeElement($page);
            // set the owning side to null (unless already changed)
            if ($page->getParent() === $this) {
                $page->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft): void
    {
        $this->lft = $lft;
    }

    /**
     * @return null|Collection
     */
    public function getMenuItems()
    {
        return $this->menuItems;
    }

    /**
     * @param ArrayCollection $menuItems
     */
    public function setMenuItems(ArrayCollection $menuItems): void
    {
        $this->menuItems = $menuItems;
    }

    public function addMenuItem(CmsMenuItem $menuItem): self
    {
        if (!$this->menuItems->contains($menuItem)) {
            $this->menuItems[] = $menuItem;
            $menuItem->setPage($this);
        }

        return $this;
    }

    public function removeMenuItem(CmsMenuItem $menuItem): self
    {
        if ($this->menuItems->contains($menuItem)) {
            $this->menuItems->removeElement($menuItem);
            // set the owning side to null (unless already changed)
            if ($menuItem->getPage() === $this) {
                $menuItem->setPage(null);
            }
        }

        return $this;
    }

    public function getSeoSmoValue($name, $default = null)
    {
        $method = 'get' . ucfirst($name);

        $value = null;
        if (method_exists($this, $method)) {
            $value = call_user_func_array([$this, $method], []);
        }

        if ($value) {
            return $value;
        }

        return $this->getParent() ? $this->getParent()->getSeoSmoValue($method, $default) : $default;
    }
}
