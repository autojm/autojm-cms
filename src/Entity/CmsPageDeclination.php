<?php

namespace AutoJM\CmsBundle\Entity;

use AutoJM\CmsBundle\Utils\SmoOpenGraphTrait;
use AutoJM\CmsBundle\Utils\SmoTwitterTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="AutoJM\CmsBundle\Repository\CmsPageDeclinationRepository")
 * @ORM\Table(name="cms__page_declination")
 */
class CmsPageDeclination
{
    use SeoAwareTrait;
    use SmoOpenGraphTrait;
    use SmoTwitterTrait;


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var CmsPage
     *
     * @ORM\ManyToOne(targetEntity="AutoJM\CmsBundle\Entity\CmsPage", inversedBy="declinations")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $page;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     */
    private $technic_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $locale;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="AutoJM\CmsBundle\Entity\CmsContent", mappedBy="declination", cascade={"remove", "persist"})
     */
    private $contents;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", length=255, nullable=false, options={"default": false})
     */
    private $active = false;

    /**
     * @var string
     * @ORM\Column(type="text", length=255, nullable=false)
     */
    private $params = '[]';

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        $this->contents = new ArrayCollection();
        $this->setActive(false);
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return (string)$this->getTitle();
    }

    public function getPath()
    {
        $params = json_decode($this->getParams(), true);
        $pagePath = $this->getPage()->getRoute()->getPath();
        $path     = preg_replace_callback('/\{(\w+)\}/', function ($matches) use ($params) {
            return $params[$matches[1]] ?? '';
        }, $pagePath);
        return preg_replace('/\/*$/', '', $path);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getContents()
    {
        return $this->contents;
    }

    public function getContent($code): ?CmsContent
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('code', $code));

        $content = $this->contents->matching($criteria)->first() ?: null;

        return $content && $content->getValue() ? $content : $this->getParentContent($code);
    }

    private function getParentContent($code)
    {
        return $this->getParent()->getContent($code);
    }

    /**
     * @param ArrayCollection $contents
     */
    public function setContents(ArrayCollection $contents): void
    {
        $this->contents = $contents;
    }

    public function addContent(CmsContent $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setDeclination($this);
        }

        return $this;
    }

    public function removeContent(CmsContent $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getDeclination() === $this) {
                $content->setDeclination(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @param mixed $page
     * @return CmsPageDeclination
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return CmsPage
     */
    public function getPage()
    {
        return $this->page;
    }

    public function getSite()
    {
        return $this->page->getSite();
    }

    /**
     * @param string $params
     * @return CmsPageDeclination
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return string
     */
    public function getParams(): string
    {
        return $this->params;
    }

    public function getSeoTitle(): ?string
    {
        if ($this->seo_title === null) {
            return '';
        }

        return $this->seo_title;
    }

    public function getTechnicName(): ?string
    {
        return $this->technic_name;
    }

    public function setTechnicName(?string $technic_name): self
    {
        $this->technic_name = $technic_name;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getParent()
    {
        return $this->getPage();
    }

    public function getRoute()
    {
        return $this->page->getRoute();
    }

    public function getSeoSmoValue($name, $default = null)
    {
        $method = 'get' . ucfirst($name);

        $value = null;
        if (method_exists($this, $method)) {
            $value = call_user_func_array([$this, $method], []);
        }

        if ($value) {
            return $value;
        }

        return $this->getPage() ? $this->getPage()->getSeoSmoValue($method, $default) : $default;
    }
}
