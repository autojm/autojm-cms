<?php

namespace AutoJM\CmsBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @UniqueEntity("path")
 * @ORM\Entity(repositoryClass="AutoJM\CmsBundle\Repository\CmsRouteRepository")
 * @ORM\Table(name="cms__route")
 */
abstract class AbstractCmsRoute implements CmsRouteInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     */
    private $name;


    /**
     * @var array
     * @ORM\Column(type="array", nullable=false)
     *
     */
    private $methods = [];


    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     */
    private $path;


    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $controller;

    /**
     * @var CmsPage
     *
     * @ORM\OneToOne(targetEntity="AutoJM\CmsBundle\Entity\CmsPage", mappedBy="route", cascade={"remove"})
     */
    private $page;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     *
     */
    private $defaults;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     *
     */
    private $requirements;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $entities;

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    public function isDynamic(): bool
    {
        return (bool) preg_match('/\{.*\}/', $this->getPath());
    }

    public function getParams()
    {
        preg_match_all('/\{(\w+)\}/', $this->getPath(), $matches);
        return $matches[1];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMethods(): ?array
    {
        return $this->methods;
    }

    public function setMethods(array $methods): self
    {
        $this->methods = $methods;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getAttributes(): array
    {
        $regex = '/\{([a-zA-Z0-9_]+)\}/';
        preg_match_all($regex, $this->path, $params);

        return $params[1] ?? [];
    }

    /**
     * @return CmsPage
     */
    public function getPage(): ?CmsPage
    {
        return $this->page;
    }

    /**
     * @param CmsPage $page
     */
    public function setPage(?CmsPage $page): void
    {
        $this->page = $page;
    }

    /**
     * @return string
     */
    public function getController(): ?string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController(?string $controller): void
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getDefaults(): ?string
    {
        return $this->defaults;
    }

    /**
     * @param string $defaults
     * @return self
     */
    public function setDefaults(?string $defaults): self
    {
        $this->defaults = $defaults;
        return $this;
    }

    public function normalizeDefaults(): array
    {
        if (!$this->getDefaults()) {
            return [];
        }

        $defaults = json_decode($this->getDefaults(), true);
        foreach ($defaults as $key => $default) {
            if (empty($default) || $default === '*') {
                unset($defaults[$key]);
            }
        }

        return $defaults;
    }

    /**
     * @return string
     */
    public function getRequirements(): ?string
    {
        return $this->requirements;
    }

    /**
     * @param string $requirements
     * @return self
     */
    public function setRequirements(?string $requirements): self
    {
        $this->requirements = $requirements;
        return $this;
    }

    public function normalizeRequirements(): array
    {
        if (!$this->getRequirements()) {
            return [];
        }

        $requirements = json_decode($this->getRequirements(), true);
        foreach ($requirements as $key => $requirement) {
            if (empty($requirement) || $requirement === '*') {
                unset($requirements[$key]);
            }
        }

        return $requirements;
    }

    /**
     * @return string
     */
    public function getEntities(): ?string
    {
        return $this->entities;
    }

    /**
     * @param string $entities
     * @return self
     */
    public function setEntities(?string $entities): self
    {
        $this->entities = $entities;
        return $this;
    }

    public function normalizeEntities(): array
    {
        $entities = json_decode($this->entities, true, 512);
        if (!$entities) {
            return [];
        }

        $list = [];
        foreach ($entities as $k => $entity) {
            if (!$entity) {
                continue;
            }

            $ex = explode('::', $entity);
            $e = $ex[0];
            $p = $ex[1] ?? null;

            $list[$k] = [
                'entity' => $e,
                'property' => $p
            ];
        }

        return $list;
    }

    public function getListEntities(): array
    {
        $list = $this->normalizeEntities();

        return array_map(static fn ($l) => $l['entity'], $list);
    }
}
