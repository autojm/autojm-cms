<?php

namespace AutoJM\CmsBundle\EventListener;

use AutoJM\CmsBundle\Entity\CmsRoute;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class RouteAdminListener
{
    /**
     * @var Router
     */
    protected $router;
    /**
     * @var Filesystem
     */
    protected $fs;
    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * RouteAdminListener constructor.
     * @param Router $router
     * @param Filesystem $fs
     * @param KernelInterface $kernel
     */
    public function __construct(Router $router, Filesystem $fs, KernelInterface $kernel)
    {
        $this->router = $router;
        $this->fs     = $fs;
        $this->kernel = $kernel;
    }

    public function postUpdate($event)
    {
        $route = $event->getObject();

        if (!$route instanceof CmsRoute) {
            return;
        }

        $this->warmUpRouteCache();
    }

    protected function warmUpRouteCache()
    {
        $cacheDir = $this->kernel->getCacheDir();

        foreach (['matcher_cache_class', 'generator_cache_class'] as $option) {
            $className = $this->router->getOption($option);
            $cacheFile = $cacheDir . DIRECTORY_SEPARATOR . $className . '.php';
            $this->fs->remove($cacheFile);
        }

        $this->router->warmUp($cacheDir);
    }
}
