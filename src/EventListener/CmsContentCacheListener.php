<?php

namespace AutoJM\CmsBundle\EventListener;

use AutoJM\CmsBundle\Event\UpdateContentEvent;
use AutoJM\CmsBundle\Services\CmsContentCacheService;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CmsContentCacheListener implements EventSubscriberInterface
{
    private CmsContentCacheService $cmsContentCacheService;

    public function __construct(CmsContentCacheService $cmsContentCacheService)
    {
        $this->cmsContentCacheService = $cmsContentCacheService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UpdateContentEvent::class => "deleteCacheContent",
        ];
    }

    public function deleteCacheContent(UpdateContentEvent $event): void
    {
        $content = $event->getContent();
        try {
            // $this->cmsContentCacheService->removeCmsContentCache($content);
            $this->cmsContentCacheService->removeAll();
        } catch (Exception $e) {
        }
    }
}
