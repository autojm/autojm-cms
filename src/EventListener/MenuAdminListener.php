<?php


namespace AutoJM\CmsBundle\EventListener;

use AutoJM\CmsBundle\Entity\CmsMenu;
use AutoJM\CmsBundle\Entity\CmsMenuItem;

class MenuAdminListener
{
    public function postPersist($event)
    {
        $menu = $event->getEntity();

        if (!$menu instanceof CmsMenu) {
            return;
        }

        if ($menu->initRoot) {
            $em = $event->getEntityManager();

            $root = new CmsMenuItem();
            $root->setName('root ' . $menu->getSite() . ' ' . $menu->getLabel());
            $root->setMenu($menu);

            $em->persist($root);
            $em->flush();
        }
    }

}
