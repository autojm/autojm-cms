<?php

namespace AutoJM\CmsBundle\EventListener;

use AutoJM\CmsBundle\Entity\CmsContent;
use AutoJM\CmsBundle\Entity\CmsSharedBlock;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use AutoJM\CmsBundle\Event\UpdateContentEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CmsContentListener implements EventSubscriber
{
    private EventDispatcherInterface $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postUpdate
        ];
    }

    public function postUpdate(PostUpdateEventArgs $args): void
    {
        if (
            !$args->getObject() instanceof CmsContent &&
            !$args->getObject() instanceof CmsSharedBlock
        ) {
            return;
        }

        $this->dispatcher->dispatch(new UpdateContentEvent($args->getObject()));
    }
}
