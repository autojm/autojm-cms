<?php

namespace AutoJM\CmsBundle\EventListener;

use Symfony\Component\HttpKernel\Event\ControllerEvent;
use AutoJM\CmsBundle\Controller\BaseCmsController;
use AutoJM\CmsBundle\Services\CmsContentVars;
use AutoJM\CmsBundle\Services\CmsHelper;
use AutoJM\CmsBundle\Services\CmsPageService;
use AutoJM\CmsBundle\Services\TemplateProvider;
use Exception;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CmsControllerListener
{
    protected $helper;
    protected $provider;
    protected $cmsConfig;
    protected $pageService;
    protected $contentVars;

    /**
     * CmsControllerListener constructor.
     * 
     * @param CmsHelper $cmsHelper
     * @param TemplateProvider $provider
     * @param $cmsConfig
     * @param CmsPageService $pageService
     */
    public function __construct(
        CmsHelper $cmsHelper,
        TemplateProvider $provider,
        $cmsConfig,
        CmsPageService $pageService,
        CmsContentVars $contentVars
    ) {
        $this->helper       = $cmsHelper;
        $this->provider     = $provider;
        $this->cmsConfig    = $cmsConfig;
        $this->pageService  = $pageService;
        $this->contentVars  = $contentVars;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }
        $request = $event->getRequest();

        if ($controller instanceof BaseCmsController) {
            if (!$this->helper->isGranted($request)) {
                throw new AccessDeniedHttpException();
            }

            $page        = $this->helper->getPage($request);
            $declinaison = $this->helper->getDeclinaison($request);
            $this->pageService->setCurrentPage($page);
            $this->pageService->setCurrentDeclinaison($declinaison);

            $route = $page->getRoute();
            try {
                $this->contentVars->inject($route, $request->attributes->all());
            } catch (Exception $e) {
                throw new NotFoundHttpException('', $e);
            }

            $locale = $this->helper->getLocale($request);
            if (!empty($locale)) {
                $request->setLocale($locale);
                $controller->setLocale($locale);
            }

            $controller->setPage($page);
            $controller->setCmsPage($this->pageService);
            $controller->setGranted($this->helper->isGranted($request));
            $controller->setProvider($this->provider);
            $controller->setGlobalVars($this->contentVars);
            $controller->setCmsConfig($this->cmsConfig);
        }
    }
}
