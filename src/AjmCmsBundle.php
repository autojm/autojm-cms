<?php

namespace AutoJM\CmsBundle;

use AutoJM\CmsBundle\DependencyInjection\Compiler\CmsContentVarsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * References:
 * @link http://symfony.com/doc/current/book/bundles.html
 */
class AjmCmsBundle extends Bundle
{
  public function build(ContainerBuilder $container)
  {
    $container->addCompilerPass(new CmsContentVarsPass());
  }
}
