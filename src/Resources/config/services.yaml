parameters:
  cms.admin.cms_page_listener.class: AutoJM\CmsBundle\EventListener\PageAdminListener
  cms.admin.cms_site_listener.class: AutoJM\CmsBundle\EventListener\SiteAdminListener
  cms.admin.cms_shared_block_listener.class: AutoJM\CmsBundle\EventListener\SharedBlockAdminListener

services:
  cms.user.validator.password_strength:
    class: AutoJM\CmsBundle\Validator\Constraints\PasswordStrengthValidator
    arguments:
      - "@translator"
    tags:
      - { name: validator.constraint_validator, alias: password_strength }

  AutoJM\CmsBundle\Collectors\CmsCollector:
    tags:
      - name: data_collector
        template: "AjmCmsBundle:collectors:template.html.twig"
        id: "cms.collector"
    arguments:
      - "@cms.helper"
      - "@cms.page_provider"
      - "@cms.admin.cms_page"
      - "%ajm_cms.cms%"

  cms.routing_loader:
    class: AutoJM\CmsBundle\Routing\ExtraLoader
    public: true
    arguments:
      ["@doctrine.orm.entity_manager", "@parameter_bag", "%ajm_cms.cms%"]
    tags:
      - { name: routing.loader }

  AutoJM\CmsBundle\Utils\SitemapIterator:
    arguments:
      - "@doctrine.orm.entity_manager"
      - "@router"

  cms.security.editable_role_builder:
    class: AutoJM\CmsBundle\Security\EditableRolesBuilder
    arguments:
      - "@security.token_storage"
      - "@security.authorization_checker"
      - "@sonata.admin.pool"
      - "%ajm_cms.cms%"
      - "%security.role_hierarchy.roles%"

  cms.helper:
    class: AutoJM\CmsBundle\Services\CmsHelper
    arguments:
      [
        "@doctrine.orm.entity_manager",
        "@cms.page_provider",
        "@twig",
        "@security.authorization_checker",
        "@security.role_hierarchy",
        "@security.token_storage",
      ]
    public: true

  # ===============================================
  # CMS provider
  # ===============================================

  AutoJM\CmsBundle\Services\TemplateProvider:
    arguments:
      $config: "%ajm_cms.templates%"

  cms.page_provider:
    alias: AutoJM\CmsBundle\Services\TemplateProvider
    public: true

  cms.block_provider:
    class: AutoJM\CmsBundle\Services\TemplateProvider
    arguments:
      $config: "%ajm_cms.shared_block%"

  # ===============================================
  # CMS services
  # ===============================================

  AutoJM\CmsBundle\Services\CmsContentService:
    arguments:
      - "@twig"
      - '@AutoJM\CmsBundle\Repository\CmsSharedBlockRepository'
      - "@cms.block_provider"
      - "@cms.current_pool"
      - '@AutoJM\CmsBundle\Repository\CmsContentRepository'
      - "@cms.global_vars"

  cms.global_vars:
    alias: AutoJM\CmsBundle\Services\CmsContentVars
    public: true

  AutoJM\CmsBundle\Services\CmsContentVars:
    autowire: true

  AutoJM\CmsBundle\Services\CmsContentCacheService:
    autowire: true

  cms.cache_cache:
    alias: AutoJM\CmsBundle\Services\CmsContentCacheService
    public: true

  AutoJM\CmsBundle\Services\CmsPageService:
    autowire: true

  cms.current_page:
    alias: AutoJM\CmsBundle\Services\CmsPageService
    public: true

  cms.content:
    alias: AutoJM\CmsBundle\Services\CmsContentService

  cms.current_pool:
    alias: AutoJM\CmsBundle\Services\CustomContentPool

  AutoJM\CmsBundle\Services\CustomContentPool:
    arguments:
      - "%ajm_cms.custom_contents%"
      - "@service_container"

  AutoJM\CmsBundle\Services\CacheProvider\PsrCacheAdapter:
    autowire: true

  AutoJM\CmsBundle\Services\CacheProvider\CacheProviderInterface:
    alias: AutoJM\CmsBundle\Services\CacheProvider\PsrCacheAdapter

  # ===============================================
  # Menu
  # ===============================================

  AutoJM\CmsBundle\Admin\BreadcrumbsBuilder\PageBreadcrumbsBuilder:
    autowire: true
    public: true

  AutoJM\CmsBundle\Admin\BreadcrumbsBuilder\MenuBreadcrumbsBuilder:
    autowire: true
    public: true

  # ===============================================
  # CKEditor Renderer
  # ===============================================

  fos_ck_editor.renderer:
    class: AutoJM\CmsBundle\Services\CKEditorRenderer
    arguments:
      - "@fos_ck_editor.builder.json_builder"
      - "@router"
      - "@assets.packages"
      - "@request_stack"
      - "@=container.has('twig') ? container.get('twig') : container.get('templating')"
      - "@=container.hasParameter('locale') ? parameter('locale') : null"
    public: true

  # ===============================================
  # TWIG EXTENSION
  # ===============================================

  cms.twig.extension:
    class: AutoJM\CmsBundle\Twig\CmsTwigExtension
    arguments:
      - "@doctrine.orm.entity_manager"
      - "@router"
      - "@twig"
      - "@cms.page_provider"
      - "@request_stack"
      - "@parameter_bag"
      - "@cms.current_page"
      - '@AutoJM\CmsBundle\Services\BreadcrumbService'
    tags:
      - { name: twig.extension }
    public: true

  cms_cache.twig.extension:
    class: AutoJM\CmsBundle\Twig\CmsCacheExtension
    arguments:
      - "@cms.cache_cache"
    tags:
      - { name: twig.extension }
    public: true

  AutoJM\CmsBundle\Services\BreadcrumbService:
    autowire: true

  AutoJM\CmsBundle\Twig\CustomContent\CustomContentTokenParser:
    tags: [{ name: "twig.token_parser" }]

  AutoJM\CmsBundle\Twig\SharedBlock\SharedBlockTokenParser:
    tags: [{ name: "twig.token_parser" }]

  # ===============================================
  # REPOSITORY
  # ===============================================

  AutoJM\CmsBundle\Repository\:
    resource: "../../Repository"
    autowire: true
    tags: ["doctrine.repository_service"]

  AutoJM\CmsBundle\Controller\SitemapController:
    arguments:
      - '@AutoJM\CmsBundle\Repository\CmsSiteRepository'
    tags: ["controller.service_arguments"]
