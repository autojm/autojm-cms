<?php

namespace AutoJM\CmsBundle\DependencyInjection\Compiler;

use AutoJM\CmsBundle\Services\CmsContentVars;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CmsContentVarsPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(CmsContentVars::class)) {
            return;
        }

        $definition = $container->findDefinition(CmsContentVars::class);
        $varConfig =  $container->getParameter('ajm_cms.vars');

        $definition->addMethodCall('setEntitiesVars', [$varConfig['entities'] ?? []]);
        if ($varConfig['global_service']) {
            $definition->addMethodCall('addGlobalVars', [new Reference($varConfig['global_service'])]);
        }
    }
}
