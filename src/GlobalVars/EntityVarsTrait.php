<?php

namespace AutoJM\CmsBundle\GlobalVars;

trait EntityVarsTrait
{
    protected array $objects = [];
    protected array $routeParams = [];

    public static function getAvailableVars(): array
    {
        return [];
    }

    public function __call($name, $arguments)
    {
        $name = lcfirst(preg_replace(['/^get/', '/^is/'], '', $name));
        $key  = array_search($name, self::getAvailableVars(), true);
        $name = is_string($key) ? $key : $name;

        $value = $this->getValue($this, $name);
        if ($value) {
            return $value;
        }

        if (empty($this->objects)) {
            return '';
        }

        $data   = explode('.', $name);
        $prefix = array_shift($data);

        $object = $this->objects[$prefix] ?? null;
        if (!$object) {
            return '';
        }

        if (!is_object($object)) {
            return $object;
        }

        $name   = $data[0];

        return $this->getValue($object, $name);
    }

    public function getObjects(): array
    {
        return $this->objects;
    }

    public function getRouteParam($param)
    {
        return $this->routeParams[$param] ?? null;
    }

    private function getValue($object, $name)
    {
        $method = 'get' . ucfirst($name);
        if (method_exists($object, $method)) {
            return $object->$method();
        }

        $method = 'is' . ucfirst($name);
        if (method_exists($object, $method)) {
            return $object->$method();
        }

        if (method_exists($object, $name)) {
            return $object->$name();
        }

        if (property_exists($object, $name)) {
            return $object->$name;
        }

        return '';
    }
}
