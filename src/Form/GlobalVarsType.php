<?php

namespace AutoJM\CmsBundle\Form;

use AutoJM\CmsBundle\Entity\CmsPage;
use AutoJM\CmsBundle\Services\CmsContentVars;
use AutoJM\CmsBundle\Services\TemplateProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TemplateType
 *
 * Type of template defined in ajm_cms.yaml
 */
class GlobalVarsType extends AbstractType
{
    protected $globalVars;
    protected $globalVarsDefinition;
    protected TemplateProvider $pageProvider;

    public function __construct(TemplateProvider $pageProvider, ContainerInterface $container, $globalVarsDefinition)
    {
        $this->pageProvider = $pageProvider;
        $this->globalVarsDefinition = $globalVarsDefinition;
        $this->globalVars   = $container->get($globalVarsDefinition['global_service']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        /** @var CmsPage $page */
        $page       = $options['page'];
        $config     = $this->pageProvider->getConfigurationFor($page->getTemplate());
        $vars       = !empty($config['entityVars']) ? $config['entityVars']::getAvailableVars() : [];

        $route = $page->getRoute();
        $routeEntities = $route->getListEntities();
        $entitiesVars = $this->globalVarsDefinition['entities'] ?? [];
        foreach ($route->getAttributes() as $param) {
            if (isset($routeEntities[$param])) {
                $entity = $routeEntities[$param];
                $entityVar = $entitiesVars[$entity] ?? null;
                if ($entityVar) {
                    $vars = array_merge($entityVar::getAvailableVars(), $vars);
                }
            } else {
                $vars = array_merge([$param], $vars);
            }
        }
        $vars = array_merge($this->globalVars::getAvailableVars(), $vars);

        $d          = CmsContentVars::getDelimiters();
        $vars = array_map(static function ($value) use ($d) {
            return $d['s'] . $value . $d['e'];
        }, $vars);

        $view->vars['user_vars'] = $vars;

        parent::buildView($view, $form, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'page' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'cms_global_vars';
    }

    public function getParent()
    {
        return TextType::class;
    }
}
