<?php

namespace AutoJM\CmsBundle\Form;

use AutoJM\CmsBundle\Services\TemplateProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TemplateType
 *
 * Type of template defined in ajm_cms.yaml
 */
class PageTemplateType extends AbstractType
{
    private $provider;

    public function __construct(TemplateProvider $provider)
    {
        $this->provider = $provider;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'choices' => $this->provider->getTemplateList(),
            ]
        );
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * @return TemplateProvider
     */
    public function getProvider(): TemplateProvider
    {
        return $this->provider;
    }
}
