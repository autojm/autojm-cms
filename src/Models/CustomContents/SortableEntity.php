<?php


namespace AutoJM\CmsBundle\Models\CustomContents;


class SortableEntity
{
    /**
     * @var mixed
     */
    public $entity;

    /**
     * @var int
     */
    public $position;

}
